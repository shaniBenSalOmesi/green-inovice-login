module.exports = {
    css: {
        loaderOptions: {
            sass: {
                data: `@import "public/styles/main.scss";`
            }
        }
    }
};