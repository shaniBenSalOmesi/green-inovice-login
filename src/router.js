import Vue from 'vue'
import Router from 'vue-router'
import Login from './pages/login'
import WelcomeUser from './pages/welcome-page'
import UserProfile from './pages/user-profile'

import store from './store/store'

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'login-page',
      component: Login
    },
    {
      path: '/welcome',
      name: 'welcome-page',
      component: WelcomeUser
    },
    {
      path: '/profile/:id',
      name: 'user-profile',
      component: UserProfile
    },
    {
      path: '*',
      redirect: '/login'
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.name === 'welcome-page' || to.name === 'user-profile') {
    if (!store.getters.getUser) {
      next('/login');
    }
  }
  if (to.fullPath === '/login') {
    if (store.getters.getUser) {
      next('/welcome');
    }
  }
  next();
});

export default router;


