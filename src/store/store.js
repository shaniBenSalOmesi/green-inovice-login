import Vue from 'vue'
import Vuex from 'vuex'
import userService from "../services/user-service";
import {REMOVE_LOGGED_USER, SET_LOGGED_USER, USER_LOGIN, USER_LOGOUT} from "./const/user";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loggedUser: userService.getUser() || null
  },
  getters: {
    getUser: state => state.loggedUser,
  },
  mutations: {
    [SET_LOGGED_USER]: (state, user) => {
      state.loggedUser = user;
    },
    [REMOVE_LOGGED_USER]: (state) => {
      state.loggedUser = null;
    }
  },
  actions: {
    [USER_LOGIN]: ({commit, payload}) => {
      userService.createToken(payload).then(res => {
        userService.login(res).then(res => {
          commit(SET_LOGGED_USER, res);
        })
      })
    },
    [USER_LOGOUT]: ({commit}) => {
      userService.logOut();
      commit(REMOVE_LOGGED_USER)
    }
  }
})
