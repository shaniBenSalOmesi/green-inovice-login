export const NAME_INPUT_DATA = {
    placeholder : 'מייל',
    description: 'כתובת המייל איתה נרשמת לחשבונית ירוקה'
};

export const PASSWORD_INPUT_DATA = {
    placeholder: 'סיסמה',
    description: '? שכחת סיסמה'
}