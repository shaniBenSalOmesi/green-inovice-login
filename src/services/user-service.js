import storageService from './storage-service.js'
const LOGGED_USER_STORAGE_KEY = 'logged-user';
const USER_TOKEN_STORAGE_KEY = 'token';

import axios from 'axios';

const MAIN_URL = 'https://sandbox.d.greeninvoice.co.il/api/v1/';

const apiHeader = {
    "id": "86526b35-d447-4911-83f4-7ecff11e6c04",
    "secret": "LSD6KCIg_gTTuq2NSHfw-g"
};

let apiTokenHeader = {};

let loggedUser = storageService.getItemFromStorage(LOGGED_USER_STORAGE_KEY) || null;

let token = storageService.getItemFromStorage(USER_TOKEN_STORAGE_KEY) || null;

function createToken() {
    return axios.post(MAIN_URL + `account/token`, apiHeader).then(token => {
        apiTokenHeader = _generateHeader(token.headers);
        storageService.saveToStorage(USER_TOKEN_STORAGE_KEY, apiTokenHeader);
        return apiTokenHeader;
    });
}

function getToken() {
    return token;
}

function getUser() {
    return loggedUser;
}

function _generateHeader(tokenHeaders) {
    return  {
        Authorization : 'Bearer' + tokenHeaders['x-authorization-bearer']
    };
}

function login(header) {
    return axios.get(MAIN_URL + `users/me`, {headers: header}).then(res => {
        storageService.saveToStorage(LOGGED_USER_STORAGE_KEY, res.data);
        return res.data;
    })
}

function logOut() {
    storageService.clearStorage(USER_TOKEN_STORAGE_KEY);
    storageService.clearStorage(LOGGED_USER_STORAGE_KEY);
}

export default {
    login,
    getUser,
    getToken,
    createToken,
    logOut
}