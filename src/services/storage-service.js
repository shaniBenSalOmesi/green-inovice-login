function getItemFromStorage(key) {
    return JSON.parse(localStorage.getItem(key))
}

function saveToStorage(key, val) {
    localStorage[key] = JSON.stringify(val);
}

function clearStorage(key) {
    localStorage.removeItem(key)
}

export default {
    getItemFromStorage,
    saveToStorage,
    clearStorage
}